<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;

class ApiService
{
    public function fetchAllDataFromApi()
    {

        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiYXVkIjoiYXBpIiwiaXNzIjoiL3NlY3VyaXR5IiwiZXhwIjoxNzExOTc2NTY5LCJpYXQiOjE3MTEzNzE3NjksImp0aSI6IjFjMmRmYmEwLTMxNDItNGQzOC04N2RlLWU4OTExN2QxZDZlZiJ9.fiSdwSslQUjwoZvbipNUl71wco4odN9jKHpbKAVGdoS1SX1M4NLQHdUZNXb8gYh8UnawwXPkWXBaMN98uoHzz8jTYvUNd75tFMval0ebFlL1sbq1IR_sL-Q2YaQd0CQEl1IyC9NNnU1KTmQchy97wvmI-PCyk6sO1K7HlrcBWpyXfsHh3t5hh7bmpYah2HZnBxzPZGXF88xxhxah65QiOlJSekUtBNvo78OhEliVuUKGC5fkxePJnKfcX8tcCQDWsJwdyArmAzrYHIoB4S0sG1Q5-x0wsZ2Miv3Udx8tX8v36nBWtEljHncAPSWSYr0i8PXJTZ96WcBLYsy--BYUjFR0lpBu_s4pHshQO_h4ZRiL4xIBDGlNODqLlI9Xg5u6szUCWBeOzPdRIWmzvxcHpa4X9qGSwMKqPX4kvrmChRirz5rfj9HOM5o69wlaGqO068L3YdQFBrlMoTk2WZN3RHFraaG9DAwro4iK8b3r5fvpJG4gTEOw8SYcMITZWbgbFKh6WJZ1nYnkAnvzGBazTvoQGhsJj1mRxk0oXakJlY0XNiIIndqStq0bXQoYhPvDRV7wZSvX7KcdGR0JKoPUVA';
        $client = HttpClient::create();
        $response = $client->request('GET', 'http://localhost:8080/api-0.0.1/api/Bids', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);
        
        return $response->getContent();
        
    }

    public function fetchDataFromApi($id)
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiYXVkIjoiYXBpIiwiaXNzIjoiL3NlY3VyaXR5IiwiZXhwIjoxNzExOTc2NTY5LCJpYXQiOjE3MTEzNzE3NjksImp0aSI6IjFjMmRmYmEwLTMxNDItNGQzOC04N2RlLWU4OTExN2QxZDZlZiJ9.fiSdwSslQUjwoZvbipNUl71wco4odN9jKHpbKAVGdoS1SX1M4NLQHdUZNXb8gYh8UnawwXPkWXBaMN98uoHzz8jTYvUNd75tFMval0ebFlL1sbq1IR_sL-Q2YaQd0CQEl1IyC9NNnU1KTmQchy97wvmI-PCyk6sO1K7HlrcBWpyXfsHh3t5hh7bmpYah2HZnBxzPZGXF88xxhxah65QiOlJSekUtBNvo78OhEliVuUKGC5fkxePJnKfcX8tcCQDWsJwdyArmAzrYHIoB4S0sG1Q5-x0wsZ2Miv3Udx8tX8v36nBWtEljHncAPSWSYr0i8PXJTZ96WcBLYsy--BYUjFR0lpBu_s4pHshQO_h4ZRiL4xIBDGlNODqLlI9Xg5u6szUCWBeOzPdRIWmzvxcHpa4X9qGSwMKqPX4kvrmChRirz5rfj9HOM5o69wlaGqO068L3YdQFBrlMoTk2WZN3RHFraaG9DAwro4iK8b3r5fvpJG4gTEOw8SYcMITZWbgbFKh6WJZ1nYnkAnvzGBazTvoQGhsJj1mRxk0oXakJlY0XNiIIndqStq0bXQoYhPvDRV7wZSvX7KcdGR0JKoPUVA';
        $client = HttpClient::create();
        $response = $client->request('GET', 'http://localhost:8080/api-0.0.1/api/Bids/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);
        
        return $response->getContent();
        
    }

    public function DeleteDataFromApi($id)
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiYXVkIjoiYXBpIiwiaXNzIjoiL3NlY3VyaXR5IiwiZXhwIjoxNzExOTc2NTY5LCJpYXQiOjE3MTEzNzE3NjksImp0aSI6IjFjMmRmYmEwLTMxNDItNGQzOC04N2RlLWU4OTExN2QxZDZlZiJ9.fiSdwSslQUjwoZvbipNUl71wco4odN9jKHpbKAVGdoS1SX1M4NLQHdUZNXb8gYh8UnawwXPkWXBaMN98uoHzz8jTYvUNd75tFMval0ebFlL1sbq1IR_sL-Q2YaQd0CQEl1IyC9NNnU1KTmQchy97wvmI-PCyk6sO1K7HlrcBWpyXfsHh3t5hh7bmpYah2HZnBxzPZGXF88xxhxah65QiOlJSekUtBNvo78OhEliVuUKGC5fkxePJnKfcX8tcCQDWsJwdyArmAzrYHIoB4S0sG1Q5-x0wsZ2Miv3Udx8tX8v36nBWtEljHncAPSWSYr0i8PXJTZ96WcBLYsy--BYUjFR0lpBu_s4pHshQO_h4ZRiL4xIBDGlNODqLlI9Xg5u6szUCWBeOzPdRIWmzvxcHpa4X9qGSwMKqPX4kvrmChRirz5rfj9HOM5o69wlaGqO068L3YdQFBrlMoTk2WZN3RHFraaG9DAwro4iK8b3r5fvpJG4gTEOw8SYcMITZWbgbFKh6WJZ1nYnkAnvzGBazTvoQGhsJj1mRxk0oXakJlY0XNiIIndqStq0bXQoYhPvDRV7wZSvX7KcdGR0JKoPUVA';
        $client = HttpClient::create();
        $response = $client->request('DELETE', 'http://localhost:8080/api-0.0.1/api/Bids/' . $id, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);
        
        return $response->getContent();
        
    }

    public function AddDataToApi()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiYXVkIjoiYXBpIiwiaXNzIjoiL3NlY3VyaXR5IiwiZXhwIjoxNzExOTc2NTY5LCJpYXQiOjE3MTEzNzE3NjksImp0aSI6IjFjMmRmYmEwLTMxNDItNGQzOC04N2RlLWU4OTExN2QxZDZlZiJ9.fiSdwSslQUjwoZvbipNUl71wco4odN9jKHpbKAVGdoS1SX1M4NLQHdUZNXb8gYh8UnawwXPkWXBaMN98uoHzz8jTYvUNd75tFMval0ebFlL1sbq1IR_sL-Q2YaQd0CQEl1IyC9NNnU1KTmQchy97wvmI-PCyk6sO1K7HlrcBWpyXfsHh3t5hh7bmpYah2HZnBxzPZGXF88xxhxah65QiOlJSekUtBNvo78OhEliVuUKGC5fkxePJnKfcX8tcCQDWsJwdyArmAzrYHIoB4S0sG1Q5-x0wsZ2Miv3Udx8tX8v36nBWtEljHncAPSWSYr0i8PXJTZ96WcBLYsy--BYUjFR0lpBu_s4pHshQO_h4ZRiL4xIBDGlNODqLlI9Xg5u6szUCWBeOzPdRIWmzvxcHpa4X9qGSwMKqPX4kvrmChRirz5rfj9HOM5o69wlaGqO068L3YdQFBrlMoTk2WZN3RHFraaG9DAwro4iK8b3r5fvpJG4gTEOw8SYcMITZWbgbFKh6WJZ1nYnkAnvzGBazTvoQGhsJj1mRxk0oXakJlY0XNiIIndqStq0bXQoYhPvDRV7wZSvX7KcdGR0JKoPUVA';
        $client = HttpClient::create();
        $response = $client->request('POST', 'http://localhost:8080/api-0.0.1/api/Bids/', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => '{"id":9,"bid":123,"idProduct":12,"idBuyer":12,"idSeller":12,"message":"12","dateCreate":null,"dateUpdate":null,"active":true}',
        ]);
        
        return $response->getContent();
        
    }
}