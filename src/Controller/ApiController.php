<?php

namespace App\Controller;

use App\Entity\Bid;
use App\Form\BidType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Service\ApiService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class ApiController extends AbstractController
{
    private $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }
    #[Route('/api/Bids', name: 'app_api')]
    public function index(): Response
    {
        $data = $this->apiService->fetchAllDataFromApi();
        $objects = json_decode($data, true);
        return $this->render('api/index.html.twig', [
            'objects' => $objects,
        ]);
    }
    #[Route('/api/Bid/{id}', name: 'app_api_Bid')]
    public function Bid($id): Response
    {
        $data = $this->apiService->fetchDataFromApi($id);
        $objects = json_decode($data, true);
        return $this->render('api/Bid.html.twig', [
            'objt' => $objects,
        ]);
    }

    #[Route('/api/Bid/B/{id}', name: 'app_api_Bid_Delete')]
    public function DeleteBid($id): Response
    {
        $this->apiService->DeleteDataFromApi($id);
        
        return $this->redirectToRoute('app_api');
    }

    #[Route('/addBid', name: 'app_api_Bid_Add')]
    public function AddBid(Request $request, SerializerInterface $serializer): Response
    {
        
        $data = $this->apiService->fetchAllDataFromApi();
        $objects = json_decode($data, true);
        $ob = end($objects);

        

        return $this->render('api/add.html.twig', [
            'ob' => $ob,
        ]);
    }
    #[Route('/updateBid', name: 'app_api_Bid_Update')]
    public function UpdateBid(Request $request,Bid $bid, SerializerInterface $serializer): Response
    {
        $data = $this->apiService->fetchAllDataFromApi();
        $objects = json_decode($data, true);
        $ob = end($objects);
        
        return $this->render('api/add.html.twig', [
            'ob' => $ob,
        ]);
    }
}
