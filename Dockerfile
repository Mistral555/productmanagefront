# Utiliser une image de base contenant PHP et Apache
FROM php:latest

# Installer Apache
RUN apt-get update && apt-get install -y apache2

WORKDIR /var/www/html

# Copier les fichiers du projet Symfony dans le conteneur
COPY . .

# Installer les dépendances PHP nécessaires pour Symfony
RUN apt-get install -y \
        libzip-dev \
        zlib1g-dev \
    && docker-php-ext-install zip pdo_mysql

# Configurer Apache pour servir le site Symfony
RUN a2enmod rewrite

EXPOSE 80

# Commande par défaut pour exécuter Apache dans le conteneur
CMD ["apache2-foreground"]
